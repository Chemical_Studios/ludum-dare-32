package net.chemicalstudios.ld32.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import net.chemicalstudios.ld32.GameController;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 1080;
		config.height = 720;
		config.title = "Infectious - Ludum Dare 32";
		config.resizable = false;
		new LwjglApplication(new GameController(), config);
	}
}
