package net.chemicalstudios.ld32;

import java.util.LinkedList;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Human {

	public enum Gender {
		Male,
		Female
	}

	protected Gender gender;
	protected Texture textureRight;
	protected Texture textureLeft;
	protected Sprite spriteRight;
	protected Sprite spriteLeft;
	protected Vector2 position;
	protected Vector2 velocity;
	protected Random rng;
	protected int type;
	protected int updatesUntilChange;
	protected float speed = 0;
	protected Rectangle hitbox;
	protected boolean isZombie = false;
	protected int zombieLife = 750;
	boolean alive = true;
	private Texture deadTexture;
	private Sprite deadSprite;
	private GameScreen gameScreen;
	private Sound deadSound;
	private int rangeBoost;
	
	public Human(int x, int y, GameScreen gameScreen) {
		deadSound = Gdx.audio.newSound(Gdx.files.internal("sounds/ded.wav"));
		rng = new Random();

		rangeBoost = rng.nextInt(75);
		
		this.gameScreen = gameScreen;
		
		updatesUntilChange = rng.nextInt(50) + 50;

		gender = rng.nextBoolean() ? Gender.Male : Gender.Female;
		if (gender.equals(Gender.Male)) {
			type = rng.nextInt(3) + 1;
		} else {
			type = rng.nextInt(3) + 4;
		}

		textureRight = new Texture(Gdx.files.internal("sprites/humans/human_" + type + ".png"));
		spriteRight = new Sprite(textureRight);
		textureLeft = new Texture(Gdx.files.internal("sprites/humans/human_" + type + "L.png"));
		spriteLeft = new Sprite(textureLeft);

		position = new Vector2(x, y);
		velocity = new Vector2(9, -9);
		speed = rng.nextInt(3) + 1;
		velocity.x = (float) (speed/4 * (rng.nextBoolean() ? -1 : 1));
		velocity.y = (float) (speed/4 * (rng.nextBoolean() ? -1 : 1));
		hitbox = new Rectangle(position.x, position.y, spriteRight.getWidth(), spriteRight.getHeight());

		zombieLife = 750 + rng.nextInt(750);

		deadTexture = new Texture(Gdx.files.internal("sprites/misc/blood.png"));
		deadSprite = new Sprite(deadTexture);
	}

	public void update(LinkedList<Human> humans) {
		if (alive) {
			if (updatesUntilChange == 0) {
				updatesUntilChange = rng.nextInt(50) + 50;
				changeDirection(humans);
			} else {
				updatesUntilChange--;
			}

			hitbox.set(position.x, position.y, spriteRight.getWidth(), spriteRight.getHeight());

			position.x += velocity.x;
			position.y += velocity.y;


			if (position.x + spriteRight.getWidth() > Gdx.graphics.getWidth() / 4 || position.x < -Gdx.graphics.getWidth() / 4) {
				velocity.x = -velocity.x;
			}
			if (position.y + spriteRight.getHeight() > Gdx.graphics.getHeight() / 4 || position.y < -Gdx.graphics.getHeight() / 4) {
				velocity.y = -velocity.y;
			}

			if (isZombie) {
				for (int i = 0; i < humans.size(); i++) {
					if (!humans.get(i).isZombie) {
						if (humans.get(i).alive && getPosition().dst(humans.get(i).getPosition()) <= 75 + rangeBoost) {
							double angle = Math.atan2((humans.get(i).getY() - getY()), (humans.get(i).getX() - getX()));
							velocity.x = (float) Math.cos(angle) * 0.7f;
							velocity.y = (float) Math.sin(angle) * 0.7f;
						}
					}
				}
				if (zombieLife > 0) {
					zombieLife--;
				} else {
					kill();
				}
				for (int i = 0; i < humans.size(); i++) {
					if (this.hitbox.overlaps(humans.get(i).hitbox)) {
						if (!humans.get(i).isZombie && humans.get(i).alive) {
							humans.get(i).turnIntoZombie();
						}
					}
				}
			}
		}
	}

	public void setVelocity(float x, float y) {
		this.velocity.set(x, y);
	}
	
	public void turnIntoZombie() {
		if (alive) {
			isZombie = true;
			if (gender.equals(Gender.Male)) {
				textureRight = new Texture(Gdx.files.internal("sprites/zombies/zombie_male.png"));	
				textureLeft = new Texture(Gdx.files.internal("sprites/zombies/zombie_maleL.png"));	
			} else {
				textureRight = new Texture(Gdx.files.internal("sprites/zombies/zombie_female.png"));	
				textureLeft = new Texture(Gdx.files.internal("sprites/zombies/zombie_femaleL.png"));	
			}
			spriteLeft = new Sprite(textureLeft);
			spriteRight = new Sprite(textureRight);
			speed = rng.nextFloat() + 1;

			gameScreen.smokeParticles(getX(), getY(), 10, "default");
		}
	}

	public void kill() {
		alive = false;
		deadSound.play(0.5f);
	}
	
	public Vector2 getPosition() {
		return position;
	}

	public void changeDirection(LinkedList<Human> humans) {
		if (isZombie) {
			for (int i = 0; i < humans.size(); i++) {
				if (!humans.get(i).isZombie) {
					if (getPosition().dst(humans.get(i).getPosition()) <= 75 + rangeBoost) {
						double angle = Math.atan2((humans.get(i).getY() - getY()), (humans.get(i).getX() - getX()));
						velocity.x = (float) Math.cos(angle) * 0.5f;
						velocity.y = (float) Math.sin(angle) * 0.5f;
					} else {
						velocity.x = (float) (speed/4 * (rng.nextBoolean() ? -1 : 1));
						velocity.y = (float) (speed/4 * (rng.nextBoolean() ? -1 : 1));
					}
				}
			}
		} else {
			velocity.x = (float) (speed/4 * (rng.nextBoolean() ? -1 : 1));
			velocity.y = (float) (speed/4 * (rng.nextBoolean() ? -1 : 1));
		}

	}

	public Sprite getSprite() {
		if (alive ) {
			if (velocity.x > 0) {
				return spriteRight;
			} else {
				return spriteLeft;
			}
		} else {
			return deadSprite;
		}

	}

	public int getX() {
		return (int) position.x;
	}
	public int getY() {
		return (int) position.y;
	}

	public void dispose() {
		textureRight.dispose();
		textureLeft.dispose();
	}
}
