package net.chemicalstudios.ld32;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

public class SpawnParticles {
	Vector2 position;
	Vector2 velocity;
	
	Texture particleTexture;
	Sprite particleSprite;
	
	Random rng = new Random();

	int size = 10;
	
	public SpawnParticles(int x, int y, String type) {
		position = new Vector2(x, y);
		velocity = new Vector2(rng.nextInt(10) * ((rng.nextBoolean()) ? -1 : 1), rng.nextInt(10) * ((rng.nextBoolean()) ? -1 : 1));
		
		if (type.equals("red") || type.equals("green")) {
			particleTexture = new Texture(Gdx.files.internal("sprites/particles/" + type + "_particle_" + rng.nextInt(3) + ".png"));
		} else {
			particleTexture = new Texture(Gdx.files.internal("sprites/particles/particle_" + rng.nextInt(3) + ".png"));
		}
		particleSprite = new Sprite(particleTexture);
		
		size = rng.nextInt(5) + 10;
		particleSprite.setSize(size, size);
	}
	
	public boolean update() {
		particleSprite.setSize(size, size);
		
		if (velocity.x > 0) {
			velocity.x--;
		} else {
			velocity.x++;
		}	
		
		if (velocity.y > 0) {
			velocity.y--;
		} else {
			velocity.y++;
		}	
		
		position.add(velocity);
		
		size--;
		return size > 0;
	}
	
	public Sprite getSprite() {
		return particleSprite;
	}

	public float getX() {
		return position.x;
	}

	public float getY() {
		return position.y;
	}
	
	public void dispose() {
		particleTexture.dispose();
	}
}
