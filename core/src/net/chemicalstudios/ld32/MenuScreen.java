package net.chemicalstudios.ld32;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;

public class MenuScreen implements Screen {

	private Texture background;
	private Sprite bgSprite;
	private Texture playButton;
	private Sprite play;
	private Texture playButtonHover;
	private Sprite playHover;

	private FreeTypeFontGenerator fontGen;
	private FreeTypeFontParameter fontPar;
	private BitmapFont fontHeading;
	private BitmapFont fontButtons;
	private SpriteBatch batch;
	private GameController game;
	private String title = "Infectious";
	private String subTitle = "A Ludum Dare 32 Game by Chemical Studios";
	private String playStr = "Play";
	private boolean hovering = false;
	
	public MenuScreen(GameController game) {
		this.game = game;
	}
	
	@Override
	public void show() {
		background = new Texture(Gdx.files.internal("sprites/misc/background.png"));
		bgSprite = new Sprite(background);
		
		playButton = new Texture(Gdx.files.internal("sprites/misc/playButton.png"));
		play = new Sprite(playButton);
		playButtonHover = new Texture(Gdx.files.internal("sprites/misc/playButtonHover.png"));
		playHover = new Sprite(playButtonHover);
		
		
		batch = new SpriteBatch();
		
		fontGen = new FreeTypeFontGenerator(Gdx.files.internal("fonts/Montserrat-Bold.ttf"));
		fontPar = new FreeTypeFontParameter();
		fontPar.color = Color.BLACK;
		fontPar.size = 42;
		fontHeading = fontGen.generateFont(fontPar);
		fontPar.color = Color.WHITE;
		fontPar.size = 24;
		fontButtons = fontGen.generateFont(fontPar);
		
		Gdx.input.setInputProcessor(game);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);	
		
		batch.begin();
		
		play.setSize(200, 60);
		
		batch.draw(bgSprite, 0, 0, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), 1, 1, 0);
		
		fontHeading.draw(batch, title, Gdx.graphics.getWidth() / 2 - fontHeading.getBounds(title).width / 2, Gdx.graphics.getHeight() - fontHeading.getBounds(title).height);
		fontHeading.draw(batch, subTitle, Gdx.graphics.getWidth() / 2 - fontHeading.getBounds(subTitle).width / 2, Gdx.graphics.getHeight() - fontHeading.getBounds(subTitle).height * 2.3f);
		
		if (hovering) {
			
		} else {
		}
		batch.draw(((hovering) ? playHover : play), Gdx.graphics.getWidth() / 2 - play.getWidth() / 2, Gdx.graphics.getHeight() / 2 - play.getHeight() / 2, 0, 0, play.getWidth(), play.getHeight(), 1, 1, 0);
		fontButtons.draw(batch, playStr, Gdx.graphics.getWidth() / 2 - fontButtons.getBounds(playStr).width / 2, Gdx.graphics.getHeight() / 2 + fontButtons.getBounds(playStr).height / 2);
		batch.end();
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void dispose() {
		background.dispose();
		playButton.dispose();
		fontGen.dispose();
		fontHeading.dispose();
		fontButtons.dispose();
		batch.dispose();
	}
	
	public void setHovering(boolean hovering) {
		this.hovering = hovering;
	}

}
