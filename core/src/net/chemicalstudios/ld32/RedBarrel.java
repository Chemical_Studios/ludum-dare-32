package net.chemicalstudios.ld32;

import java.util.LinkedList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class RedBarrel extends Barrel {

	public RedBarrel(int x, int y, GameScreen gameScreen) {
		super(x, y, gameScreen);
		this.texture = new Texture(Gdx.files.internal("sprites/misc/red_barrel.png"));
		this.sprite = new Sprite(texture);
		this.type = "red";
	}
	
	@Override
	public boolean update(LinkedList<Human> humans) {
		super.update(humans);
		for (int i = 0; i < humans.size(); i++) {
			if (humans.get(i).hitbox.overlaps(hitbox) && humans.get(i).alive) {
				explode(type, humans);
				return false;
			}
		}
		return true;
	}
	
	@Override
	public void explode(String type, LinkedList<Human> humans) {
		super.explode(type, humans);
		
		for (int i = 0; i < humans.size(); i++) {
			if (humans.get(i).getPosition().dst(this.position) <= 50) {
				humans.get(i).kill();
			}
		}
	}
}
