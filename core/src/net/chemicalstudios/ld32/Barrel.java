package net.chemicalstudios.ld32;

import java.util.LinkedList;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Barrel {
	protected Vector2 position;
	protected Texture texture;
	protected Sprite sprite;
	private GameScreen gameScreen;
	protected Rectangle hitbox;
	protected String type;
	
	public Barrel(int x, int y, GameScreen gameScreen) {
		position = new Vector2(x, y);
		this.gameScreen = gameScreen;
		type = "default";
	}
	
	public boolean update(LinkedList<Human> humans) {
		hitbox = new Rectangle(position.x, position.y, sprite.getWidth(), sprite.getHeight());
		return true;
	}
	
	public void explode(String type, LinkedList<Human> humans) {
		gameScreen.smokeParticles((int) position.x, (int) position.y, 100, type);
	}
	
	public Sprite getSprite() {
		return sprite;
	}
	
	public void dispose() {
		texture.dispose();
	}
	
	public int getX() {
		return (int) position.x;
	}
	public int getY() {
		return (int) position.y;
	}
	
	public int getWidth() {
		return (int) sprite.getWidth();
	}
	
	public int getHeight() {
		return (int) sprite.getHeight();
	}
}
