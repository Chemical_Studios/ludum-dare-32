package net.chemicalstudios.ld32;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;

public class GameController extends Game implements InputProcessor {
	
	private GameScreen gameScreen = new GameScreen(this);
	private MenuScreen menuScreen = new MenuScreen(this);
	@Override
	public void create () {
		this.setScreen(menuScreen);
	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose() {
		gameScreen.dispose();
		super.dispose();
	}

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		
		if (this.getScreen().equals(gameScreen)) {
			if (keycode == Keys.M) {
				gameScreen.mute();
			}
		}
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		
		if (this.getScreen().equals(gameScreen)) {
			if (button == Buttons.LEFT) {
				gameScreen.leftClick();
			}
		} else {
			if (screenX > 440 && screenX < 640) {
				if (screenY > 330 && screenY < 390) {
					this.setScreen(gameScreen);
				}
			}
		}
				
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		
		if (this.getScreen().equals(menuScreen)) {
			if (screenX > 440 && screenX < 640) {
				if (screenY > 330 && screenY < 390) {
					menuScreen.setHovering(true);
				} else {
					menuScreen.setHovering(false);
				}
			} else {
				menuScreen.setHovering(false);
			}
		}
		
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
}
