package net.chemicalstudios.ld32;

import java.util.LinkedList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class GreenBarrel extends Barrel {
	public GreenBarrel(int x, int y, GameScreen gameScreen) {
		super(x, y, gameScreen);
		this.texture = new Texture(Gdx.files.internal("sprites/misc/green_barrel.png"));
		this.sprite = new Sprite(texture);	
		this.type = "green";
	}
	
	@Override
	public boolean update(LinkedList<Human> humans) {
		super.update(humans);
		for (int i = 0; i < humans.size(); i++) {
			if (humans.get(i).hitbox.overlaps(hitbox) && humans.get(i).alive) {
				explode(type, humans);
				return false;
			}
		}
		return true;
	}
	
	@Override
	public void explode(String type, LinkedList<Human> humans) {
		super.explode(type, humans);
		
		for (int i = 0; i < humans.size(); i++) {
			if (humans.get(i).getPosition().dst(this.position) <= 75) {
				humans.get(i).turnIntoZombie();
			}
		}
	}
}
