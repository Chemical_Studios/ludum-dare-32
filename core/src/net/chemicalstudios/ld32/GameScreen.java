package net.chemicalstudios.ld32;

import java.util.LinkedList;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.math.Vector3;

public class GameScreen implements Screen {

	private LinkedList<Human> humans;
	private SpriteBatch batch;
	private SpriteBatch HUD;
	private OrthographicCamera camera;
	private Random rng;
	private int zombiesAlive;
	private int zombiesLeft = 2;
	private int humansAlive;
	private int wave;
	private FreeTypeFontGenerator fontGenerator;
	private FreeTypeFontParameter fontParameter;
	private BitmapFont font0;
	private BitmapFont font1;
	private BitmapFont font2;
	private BitmapFont font3;
	private GameController game;
	private String zombiesAliveStr;
	private String zombiesLeftStr;
	private String humansAliveStr;
	private String waveStr;
	private String gameOverStr;
	private String loseStr;
	private Texture zombieHeadTexture;
	private Sprite zombieHead;
	private Texture humanHeadTexture;
	private Sprite humanHead;
	private int respawnTimer = 1000;
	private Texture backgroundTexture;
	private Sprite background;
	private Pixmap cursorImage;
	private LinkedList<SpawnParticles> particles;
	private Sound spawnSound;
	private Music bgm;
	private LinkedList<Barrel> barrels;
	
	public GameScreen(GameController game) {
		this.game = game;
	}

	@Override
	public void show() {
		bgm = Gdx.audio.newMusic(Gdx.files.internal("sounds/bgm.mp3"));
		bgm.setVolume(0.9f);
		bgm.setLooping(true);
		bgm.play();
		spawnSound = Gdx.audio.newSound(Gdx.files.internal("sounds/transform.wav"));
		
		particles = new LinkedList<SpawnParticles>();
		
		cursorImage = new Pixmap(Gdx.files.internal("sprites/misc/cursor.png"));
		Gdx.input.setCursorImage(cursorImage, cursorImage.getWidth() / 2, cursorImage.getHeight() / 2);
		
		humans = new LinkedList<Human>();

		rng = new Random();

		barrels = new LinkedList<Barrel>();
		wave = 0;
		nextWave();

		batch = new SpriteBatch();
		HUD = new SpriteBatch();
		
		camera = new OrthographicCamera(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);

		Gdx.input.setInputProcessor(game);

		fontGenerator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/Montserrat-Bold.ttf"));
		fontParameter = new FreeTypeFontParameter();
		fontParameter.color = Color.BLACK;
		fontParameter.size = 24;
		font0 = fontGenerator.generateFont(fontParameter);
		fontParameter.size = 12;
		font1 = fontGenerator.generateFont(fontParameter);
		fontParameter.size = 18;
		font2 = fontGenerator.generateFont(fontParameter);
		fontParameter.size = 48;
		font3 = fontGenerator.generateFont(fontParameter);


		zombieHeadTexture = new Texture(Gdx.files.internal("sprites/HUD/zombie_head.png"));
		zombieHead = new Sprite(zombieHeadTexture);
		zombieHead.setPosition(camera.position.x - camera.viewportWidth / 2 + (0.12f * zombieHead.getWidth()), camera.viewportHeight / 2 - zombieHead.getHeight() * 1.2f);
		humanHeadTexture = new Texture(Gdx.files.internal("sprites/HUD/human_head.png"));
		humanHead = new Sprite(humanHeadTexture);
		humanHead.setPosition(camera.position.x + camera.viewportWidth / 2 - humanHead.getWidth() * 1.2f, zombieHead.getY());

		backgroundTexture = new Texture(Gdx.files.internal("sprites/misc/background.png"));
		background = new Sprite(backgroundTexture);
		
	}

	public void updateStrings() {
		zombiesAlive = 0;
		humansAlive = 0;
		for (int i = 0; i < humans.size(); i++) {
			if (humans.get(i).isZombie && humans.get(i).alive) {
				zombiesAlive++;
			} else {
				if (humans.get(i).alive) {
					humansAlive++;
				}
			}
		}

		zombiesAliveStr = new String(zombiesAlive + " alive");
		zombiesLeftStr = new String(zombiesLeft + ((zombiesLeft == 1) ? " zombie" : " zombies") + " at your disposal");
		humansAliveStr = new String(humansAlive + " alive");
		waveStr = new String("Wave " + wave);

		if (humansAlive == 0) {
			nextWave();
		}

		if (zombiesAlive == 0 && zombiesLeft == 0) {
			gameOverStr = "Game Over.";
			loseStr = "You lost! R to restart.";
		}
	}

	public void placeBarrels() {
		for (int i = 0; i < rng.nextInt(4) + 1; i++) {
			if (rng.nextInt(100) <= 15) {
				barrels.add(new GreenBarrel(rng.nextInt(Gdx.graphics.getWidth() / 7) * (rng.nextBoolean()?-1:1), rng.nextInt(Gdx.graphics.getHeight() / 7) * (rng.nextBoolean()?-1:1), this));
			} else {
				barrels.add(new RedBarrel(rng.nextInt(Gdx.graphics.getWidth() / 7) * (rng.nextBoolean()?-1:1), rng.nextInt(Gdx.graphics.getHeight() / 7) * (rng.nextBoolean()?-1:1), this));
			}
		}
	}
	
	public void nextWave() {
		// Humans = 2*wave + 3 [1:5, 2:7, 3:9, etc, etc]
		wave++;
		zombiesLeft = 0;

		for (int i = 0; i < humans.size(); i++) {
			humans.get(i).dispose();
		}

		humans = new LinkedList<Human>();

		for (int i = 0; i < (2 * wave) + 3; i++) {
			humans.add(new Human(rng.nextInt(Gdx.graphics.getWidth() / 7) * (rng.nextBoolean()?-1:1), rng.nextInt(Gdx.graphics.getHeight() / 7) * (rng.nextBoolean()?-1:1), this));
		}

		// 1 zombie for every 10 humans
		if (humans.size() > 10) {
			zombiesLeft = (int) Math.floor((float) humans.size() / 10);
		} else {
			zombiesLeft = 1;
		}
		
		for (int i = 0; i < barrels.size(); i++) {
			barrels.removeFirst();
		}
		
		placeBarrels();
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);	

		
		updateStrings();
		camera.update();

		batch.setProjectionMatrix(camera.combined);
		HUD.setProjectionMatrix(camera.combined);
		batch.begin();
		
		batch.draw(background, -Gdx.graphics.getWidth() / 2, -Gdx.graphics.getHeight() / 2, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), 1, 1, 0);
		
		for (int i = 0; i < humans.size(); i++) {
			batch.draw(humans.get(i).getSprite(), humans.get(i).getX(), humans.get(i).getY(), 0, 0, humans.get(i).getSprite().getWidth(), humans.get(i).getSprite().getHeight(), 1, 1, 0);
			humans.get(i).update(humans);
		}
		
		for (int i = 0; i < particles.size(); i++) {
			batch.draw(particles.get(i).getSprite(), particles.get(i).getX(), particles.get(i).getY(), 0, 0, particles.get(i).getSprite().getWidth(), particles.get(i).getSprite().getHeight(), 1, 1, 0);
			if (!particles.get(i).update()) {
				particles.remove(i);
			}
		}
		
		font0.draw(batch, waveStr, camera.position.x - font0.getBounds(waveStr).width / 2, camera.viewportHeight / 2 - font0.getBounds(waveStr).height / 2);
		font1.draw(batch, zombiesAliveStr, zombieHead.getX() + (1.2f * zombieHead.getWidth()), zombieHead.getY() + (4 * font1.getBounds(zombiesAliveStr).height));
		font1.draw(batch, zombiesLeftStr, zombieHead.getX() + (1.2f * zombieHead.getWidth()), zombieHead.getY() + (2 * font1.getBounds(zombiesLeftStr).height));
		font2.draw(batch, humansAliveStr, humanHead.getX() - (1.2f * font2.getBounds(humansAliveStr).width), humanHead.getY() + (3 * font1.getBounds(humansAliveStr).height));
		if (zombiesAlive == 0 && zombiesLeft == 0) {
			font0.draw(batch, gameOverStr, camera.position.x - font0.getBounds(gameOverStr).width / 2, camera.position.y + font0.getBounds(gameOverStr).height / 2);
			font0.draw(batch, loseStr, camera.position.x - font0.getBounds(loseStr).width / 2, camera.position.y - font0.getBounds(loseStr).height / 0.9f);
		} else {
			for (int i = 0; i < barrels.size(); i++) {
				batch.draw(barrels.get(i).getSprite(), barrels.get(i).getX(), barrels.get(i).getY(), 0, 0, barrels.get(i).getWidth(), barrels.get(i).getHeight(), 1, 1, 0);
				if (!barrels.get(i).update(humans)) {
					barrels.remove(i);
				}
			}
		}

		batch.end();
		
		HUD.begin();
		HUD.setColor(1, 1, 1, 0.5f);
		HUD.draw(zombieHead, zombieHead.getX(), zombieHead.getY(), 0, 0, zombieHead.getWidth(), zombieHead.getHeight(), 1, 1, 0);
		HUD.draw(humanHead, humanHead.getX(), humanHead.getY(), 0, 0, humanHead.getWidth(), humanHead.getHeight(), 1, 1, 0);
		HUD.end();

		if (respawnTimer <= 0) {
			respawnTimer = 1000;
			for (int i = 0; i < humansAlive / 3; i++) {
				humans.add(new Human(0, 0, this));
			}
		} else {
			respawnTimer--;
		}

		if (Gdx.input.isKeyJustPressed(Keys.R)) {
			wave = 0;
			nextWave();
		}
	
	}

	public void spawnHumans(int x, int y) {
		humans.add(new Human(x, y, this));
	}


	public void spawnZombies(int x, int y) {
		if (zombiesLeft > 0) {
			humans.add(new Human(x, y, this));
			humans.getLast().turnIntoZombie();
			zombiesLeft--;
			
			smokeParticles(x, y, 10, "grey");
		}
	}

	public void smokeParticles(int x, int y, int intensity, String type) {
		for (int i = 0; i < intensity; i++) {
			particles.add(new SpawnParticles(x, y, type));
		}
		spawnSound.play(0.5f);
	}
	
	public void leftClick() {
		Vector3 newCoords = camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
		spawnZombies((int) newCoords.x, (int) newCoords.y);
	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void dispose() {
		batch.dispose();

		for (int i = 0; i < humans.size(); i++) {
			humans.get(i).dispose();
		}
		
		cursorImage.dispose();
		backgroundTexture.dispose();
		font0.dispose();
		font1.dispose();
		font2.dispose();
		font3.dispose();
		fontGenerator.dispose();
		humanHeadTexture.dispose();
		zombieHeadTexture.dispose();
		HUD.dispose();
		
		for (int i = 0; i < particles.size(); i++) {
			particles.get(i).dispose();
		}
		
		for (int i = 0; i < barrels.size(); i++) {
			barrels.get(i).dispose();
		}
		
		spawnSound.dispose();
	}

	public void mute() {
		if (bgm.isPlaying()) {
			bgm.pause();
		} else {
			bgm.play();
		}
		
	}
}
